package entornos;

/**
 * 
 * Interfaz indica si los aviones están disponibles
 */
public interface Activable {

    public boolean isActivado();

    public void setActivado(boolean value);
}
